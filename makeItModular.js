var findFiles = require('./module');

var ext = process.argv[3];
var dir = process.argv[2];

findFiles(dir, ext, function(err, files){
	if(err)
		console.log(err);
	else
		files.forEach(function(file){
			console.log(file);
		});
})