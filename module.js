var fs = require('fs');


module.exports = function ( dir, ext, cb ) {
	var extFiles = [];
	fs.readdir(dir, function(err, files){
		if(err)
			cb(err, false);
		else {
			files = files.filter(function(file){
				var name = file.split(".");
				if(name[name.length -1] == ext && name.length > 1) 
					return file;
			});
			cb(null, files);
		}

	});	
	
}	


